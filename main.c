//
// Created by Vo Dinh Nguyen  on 05/11/2023.
//

#include "main.h"
#include "stdio.h"
#include "mm_malloc.h"

static int numIdenticalPairs(int* nums, int numsSize) {
    int goodPairsNum;
    goodPairsNum = 0;
    for(int i = 0; i < numsSize; i++){
        for(int j = i+1; j < numsSize; j++){
            if(nums[i] == nums[j]){
                goodPairsNum++;
            }
        }
    }
    return goodPairsNum;
}

int main(){
    int* nums;
    int numsSize;

    nums = (int*) malloc(numsSize*sizeof(nums));
    printf("\nEnter the element in array: ");
    scanf("%d", &numsSize);

    printf("\nEnter the nums in array: ");
    for(int i = 0; i < numsSize; i++){
        scanf("%d", &nums[i]);
    }

    int ans = numIdenticalPairs(nums, numsSize);
    printf("Output: %d ", ans);

    free(nums);
    return 0;
}